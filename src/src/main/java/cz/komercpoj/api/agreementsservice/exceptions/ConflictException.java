package cz.komercpoj.api.agreementsservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ConflictException extends RuntimeException {
    public <T> ConflictException(Class cls, T urlId, T entityId) {
        super(String.format("%s - url id %s does not match payload id %s", cls.getSimpleName(), urlId, entityId));
    }
}
