package cz.komercpoj.api.agreementsservice.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Audited
@Entity
@Getter @Setter
public class Consent {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_api_consent")
    @SequenceGenerator(name = "seq_api_consent", sequenceName = "seq_api_consent", allocationSize = 1)
    private Long id;
    private String consentType;
    private Boolean value;
}
