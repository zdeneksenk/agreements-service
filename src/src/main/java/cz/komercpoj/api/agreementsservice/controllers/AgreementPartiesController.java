package cz.komercpoj.api.agreementsservice.controllers;

import cz.komercpoj.api.agreementsservice.model.Party;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("agreements/{agreementId}/parties")
public class AgreementPartiesController {

    @GetMapping
    public List<Party> getParties(@PathVariable Long agreementId) {
        //TODO: implement
        return null;
    }

    @PostMapping
    public Party createParty(@PathVariable Long agreementId, @RequestBody Party party) {
        //TODO: implement
        return null;
    }

    @GetMapping(path = "/{partyId}")
    public Party getParty(@PathVariable Long agreementId, @PathVariable Long partyId) {
        //TODO: implement
        return null;
    }

    @PutMapping(path = "/{partyId}")
    public void updateParty(@PathVariable Long agreementId, @PathVariable Long partyId, @RequestBody Party party) {
        //TODO: implement
    }
}
