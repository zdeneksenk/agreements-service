package cz.komercpoj.api.agreementsservice.mapping;

import cz.komercpoj.api.agreementsservice.model.Agreement;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

import static cz.komercpoj.api.agreementsservice.mapping.RelationsMapper.oneToOneMapping;

public class AgreementMapper extends CustomMapper<Agreement, Agreement> {
    @Override
    public void mapAtoB(Agreement agreementIn, Agreement agreementOut, MappingContext context) {
        agreementOut.setPayment(oneToOneMapping(mapperFacade, agreementIn.getPayment(), agreementOut.getPayment()));
        agreementOut.setDocument(oneToOneMapping(mapperFacade, agreementIn.getDocument(), agreementOut.getDocument()));
        agreementOut.setHealthQuestionnaire(oneToOneMapping(mapperFacade, agreementIn.getHealthQuestionnaire(), agreementOut.getHealthQuestionnaire()));

        //TODO: add tests for Document and HealthQuestionnaire create/update/refuse
    }
}
