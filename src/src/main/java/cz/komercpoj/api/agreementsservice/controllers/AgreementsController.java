package cz.komercpoj.api.agreementsservice.controllers;

import cz.komercpoj.api.agreementsservice.dtos.SimpleAgreementDto;
import cz.komercpoj.api.agreementsservice.exceptions.ConflictException;
import cz.komercpoj.api.agreementsservice.model.Agreement;
import cz.komercpoj.api.agreementsservice.services.AgreementService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("agreements")
public class AgreementsController {

    private final AgreementService agreementService;

    public AgreementsController(AgreementService agreementService) {
        this.agreementService = agreementService;
    }

    @GetMapping
    public List<SimpleAgreementDto> getAgreements(@RequestParam(required = false) Long page,
                                                  @RequestParam(required = false) Long size) {
        //TODO: define SimpleAgreementDto content
        //TODO: implement
        //TODO: define searchable fields
        //TODO: search, pagination
    	
    	List<SimpleAgreementDto> list = new ArrayList<>();
    	
    	SimpleAgreementDto dto = new SimpleAgreementDto();
    	dto.setId(System.currentTimeMillis());
    	list.add(dto);
    	
        return list; 
    }

    @PostMapping
    public ResponseEntity<Agreement> createAgreement(@RequestBody Agreement agreement) {
        //TODO: validation: make sure there are no IDs in entities to be created; there can be IDs in entities that are getting associated
        //TODO: validation: make sure mandatory data / entities are present
        return new ResponseEntity<>(agreementService.createAgreement(agreement), HttpStatus.CREATED);
    }

    @GetMapping(path = "/{agreementId}")
    public Agreement getAgreement(@PathVariable Long agreementId) {
        return agreementService.getAgreement(agreementId);
    }

    @PutMapping(path = "/{agreementId}")
    public ResponseEntity updateAgreement(@PathVariable Long agreementId, @RequestBody Agreement inAgreement) {
        if (!agreementId.equals(inAgreement.getId())) {
            throw new ConflictException(Agreement.class, agreementId, inAgreement.getId());
        }

        agreementService.updateAgreement(agreementId, inAgreement);

        return ResponseEntity.noContent().build();
    }

    @PatchMapping(path = "/{agreementId}")
    public ResponseEntity patchAgreement(@PathVariable Long agreementId, @RequestBody Agreement inAgreement) {
        agreementService.patchAgreement(agreementId, inAgreement);
        return ResponseEntity.noContent().build();
    }
}
