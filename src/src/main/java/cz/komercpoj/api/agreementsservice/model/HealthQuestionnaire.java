package cz.komercpoj.api.agreementsservice.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Audited
@Entity
@Getter @Setter
public class HealthQuestionnaire implements Identifier {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_api_health_questionnaire")
    @SequenceGenerator(name = "seq_api_health_questionnaire", sequenceName = "seq_api_health_questionnaire", allocationSize = 1)
    private Long id;
    private String weight;
    private String height;
}
