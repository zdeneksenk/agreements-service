package cz.komercpoj.api.agreementsservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class EntityIdConflictException extends RuntimeException {
    public <T> EntityIdConflictException(Class cls, T payloadId, T entityId) {
        super(String.format("%s - payload id %s does not match entity id %s", cls.getSimpleName(), payloadId, entityId));
    }
}
