package cz.komercpoj.api.agreementsservice.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Audited
@Entity
@Getter @Setter
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_api_address")
    @SequenceGenerator(name = "seq_api_address", sequenceName = "seq_api_address", allocationSize = 1)
    private Long id;
    private String addressType;
    private String street;
    private String conscriptionNumber;
    private String streetNumber;
    private String provisionalNumber;
    private String city;
    private String postCode;
    private String country;
}
