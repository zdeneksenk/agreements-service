package cz.komercpoj.api.agreementsservice.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Audited
@Entity
@Getter @Setter
public class Party {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_api_party")
    @SequenceGenerator(name = "seq_api_party", sequenceName = "seq_api_party", allocationSize = 1)
    private Long id;
    private String title;
    private String name;
    private String surname;
    private String sex;
    private String birthName;
    private String titleAfter;
    private LocalDate birthDate;
    private String birthNumber;
    private String birthPlace;
    private String birthState;
    private String citi;  //TODO: ???

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Address> addresses = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Contact> contacts = new HashSet<>();

    private String workloadType;
    private String incomeAmount;
    private String job;
}
