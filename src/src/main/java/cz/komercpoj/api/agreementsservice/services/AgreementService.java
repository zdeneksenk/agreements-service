package cz.komercpoj.api.agreementsservice.services;

import cz.komercpoj.api.agreementsservice.exceptions.NotFoundException;
import cz.komercpoj.api.agreementsservice.model.Agreement;
import cz.komercpoj.api.agreementsservice.repositories.AgreementRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AgreementService {

    private final AgreementRepository agreementRepository;
    private final MapperFacade mapperFacade;

    public AgreementService(AgreementRepository agreementRepository, MapperFacade mapperFacade) {
        this.agreementRepository = agreementRepository;
        this.mapperFacade = mapperFacade;
    }

    public Agreement getAgreement(Long id) {
        return agreementRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(Agreement.class, id));
    }

    @Transactional
    public Agreement createAgreement(Agreement agreement) {
        return agreementRepository.save(agreement);
    }

    @Transactional
    public void patchAgreement(Long agreementId, Agreement inAgreement) {
        Agreement dbAgreement = getAgreement(agreementId);
        updateStatus(dbAgreement, inAgreement.getStatus());
        agreementRepository.save(dbAgreement);
    }

    private void updateStatus(Agreement agreement, String status) {
        //TODO: workflow enforcement
        agreement.setStatus(status);
    }

    public void updateAgreement(Long agreementId, Agreement inAgreement) {
        //TODO: rules for attached entities:
        // One-to-one relation:
        //   - IN and DB has the same id = update
        //   - IN has no id = delete DB, create new one from IN
        //   - IN has different id than DB = ConflictException
        // One-to-many relation:
        //   - all DB that are not in IN by id = delete
        //   - all IN that are in DB by id = update
        //   - all IN that have no id = create
        //   - if there is any IN with id that is not in DB = ConflictException
        //     -- UNLESS IT IS ENUM WITH GLOBAL USE (we should not allow just assignment of IN with existing id because ownership will be implemented through Agreement.product + JWT and not on all entities)



    }
}
