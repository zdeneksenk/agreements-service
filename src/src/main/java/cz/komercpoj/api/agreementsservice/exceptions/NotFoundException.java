package cz.komercpoj.api.agreementsservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {
    public <T> NotFoundException(Class cls, T id) {
        super(String.format("Could not find %s with id %s", cls.getSimpleName(), id));
    }
}
