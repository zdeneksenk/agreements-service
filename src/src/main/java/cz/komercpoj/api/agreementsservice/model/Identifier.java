package cz.komercpoj.api.agreementsservice.model;

public interface Identifier {
    <T> T getId();
}
