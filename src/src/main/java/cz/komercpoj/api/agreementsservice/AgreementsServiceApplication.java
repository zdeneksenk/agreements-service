package cz.komercpoj.api.agreementsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class AgreementsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgreementsServiceApplication.class, args);
	}
}
