package cz.komercpoj.api.agreementsservice.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Audited
@Entity
@Getter @Setter
public class Agreement {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_api_agreement")
    @SequenceGenerator(name = "seq_api_agreement", sequenceName = "seq_api_agreement", allocationSize = 1)
    private Long id;

    private Long agreementNumber;
    private LocalDate agreementBeginDate;
    private LocalDate agreementEndDate;
    private String insurancePeriod;
    private String insuranceTermsVersion;
    private LocalDate signDate;
    private String bruttoPremium;
    private String premium;
    private String status;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Payment payment;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Document document;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private HealthQuestionnaire healthQuestionnaire;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Party> parties = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Coverage> coverages = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Beneficiary> beneficiaries = new HashSet<>();
}
