package cz.komercpoj.api.agreementsservice.mapping;

import cz.komercpoj.api.agreementsservice.model.Agreement;
import ma.glasnost.orika.MapperFactory;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;
import org.springframework.stereotype.Component;

@Component
public class MapperConfiguration implements OrikaMapperFactoryConfigurer {
    @Override
    public void configure(MapperFactory mapperFactory) {
        mapperFactory.classMap(Agreement.class, Agreement.class)
                .exclude("id")
                .exclude("payment")
                .exclude("document")
                .exclude("healthQuestionnaire")
                .exclude("parties")
                .exclude("coverages")
                .exclude("beneficiaries")
                .customize(new AgreementMapper())
                .byDefault()
                .register();
    }
}
