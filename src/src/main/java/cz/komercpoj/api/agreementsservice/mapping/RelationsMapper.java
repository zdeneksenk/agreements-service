package cz.komercpoj.api.agreementsservice.mapping;

import cz.komercpoj.api.agreementsservice.exceptions.EntityIdConflictException;
import cz.komercpoj.api.agreementsservice.model.Identifier;
import ma.glasnost.orika.MapperFacade;

public class RelationsMapper {
    public static <T, U>  T oneToOneMapping(MapperFacade mapperFacade, T fieldIn, T fieldOut) {
        if (fieldIn == null) {
            return fieldOut;
        }

        U inId = ((Identifier)fieldIn).getId();
        U outId = ((Identifier)fieldOut).getId();

        if (inId == null) {
            return fieldIn;
        }

        if (!inId.equals(outId)) {
            throw new EntityIdConflictException(fieldOut.getClass(), inId, outId);
        }

        mapperFacade.map(fieldIn, fieldOut);
        return fieldOut;
    }
}
