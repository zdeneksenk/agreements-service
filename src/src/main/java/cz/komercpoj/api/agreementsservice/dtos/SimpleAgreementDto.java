package cz.komercpoj.api.agreementsservice.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SimpleAgreementDto {
    private Long id;
}
