package cz.komercpoj.api.agreementsservice.repositories;

import cz.komercpoj.api.agreementsservice.model.Agreement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgreementRepository extends CrudRepository<Agreement, Long> {
}
