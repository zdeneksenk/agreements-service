package cz.komercpoj.api.agreementsservice.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.time.LocalDate;

@Audited
@Entity
@Getter @Setter
public class Document implements Identifier {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_api_document")
    @SequenceGenerator(name = "seq_api_document", sequenceName = "seq_api_document", allocationSize = 1)
    private Long id;
    private String type;
    private String number;
    private LocalDate issueDate;
    private LocalDate expirationDate;
    private String issueState;
    private String issuedBy;
}
