package cz.komercpoj.api.agreementsservice.configuration;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/**").permitAll();

        // csrf and frameOptions security disabled to be able to access h2 db console, for playing with data
        // TODO: remove later
        http.csrf().disable();
        http.headers().frameOptions().disable();
    }
}
