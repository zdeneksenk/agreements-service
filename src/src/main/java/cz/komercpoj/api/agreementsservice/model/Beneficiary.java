package cz.komercpoj.api.agreementsservice.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Audited
@Entity
@Getter @Setter
public class Beneficiary {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_api_beneficiary")
    @SequenceGenerator(name = "seq_api_beneficiary", sequenceName = "seq_api_beneficiary", allocationSize = 1)
    private Long id;
    private String firstName;
    private String surname;
    private LocalDate birthDate;
    private String relationship;
    private String ratio;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Address> addresses = new HashSet<>();
}
