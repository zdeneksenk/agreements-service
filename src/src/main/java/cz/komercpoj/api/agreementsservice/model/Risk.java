package cz.komercpoj.api.agreementsservice.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Audited
@Entity
@Getter @Setter
public class Risk {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_api_risk")
    @SequenceGenerator(name = "seq_api_risk", sequenceName = "seq_api_risk", allocationSize = 1)
    private Long id;
    private String code;
}
