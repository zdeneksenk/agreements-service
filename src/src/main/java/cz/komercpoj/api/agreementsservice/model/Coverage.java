package cz.komercpoj.api.agreementsservice.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Audited
@Entity
@Getter @Setter
public class Coverage {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_api_coverage")
    @SequenceGenerator(name = "seq_api_coverage", sequenceName = "seq_api_coverage", allocationSize = 1)
    private Long id;
    private String sumInsured;
    private String premium;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Risk risk;
}
