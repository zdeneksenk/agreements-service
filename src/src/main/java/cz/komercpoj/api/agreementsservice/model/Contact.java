package cz.komercpoj.api.agreementsservice.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Audited
@Entity
@Getter @Setter
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_api_contact")
    @SequenceGenerator(name = "seq_api_contact", sequenceName = "seq_api_contact", allocationSize = 1)
    private Long id;
    private String contactType;
}
