package cz.komercpoj.api.agreementsservice.tests;

import cz.komercpoj.api.agreementsservice.builders.AgreementBuilder;
import cz.komercpoj.api.agreementsservice.builders.BeneficiaryBuilder;
import cz.komercpoj.api.agreementsservice.builders.CoverageBuilder;
import cz.komercpoj.api.agreementsservice.builders.PartyBuilder;
import cz.komercpoj.api.agreementsservice.model.Agreement;
import cz.komercpoj.api.agreementsservice.repositories.AgreementRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicAddress1;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicAddress2;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicAgreement;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicBeneficiary;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicContact;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicCoverage;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicDocument;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicHealthQuestionnaire;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicParty;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicPayment;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicRisk;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = {"spring.datasource.url=jdbc:h2:mem:PatchAgreementDB"})
public class PatchAgreementTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private AgreementRepository agreementRepository;

    private static boolean isInitialized;

    @Before
    public void setUp() {
        if (isInitialized) {
            return;
        }

        isInitialized = true;
        loadData();
    }

    private void loadData() {
        Agreement agreement = AgreementBuilder.with(basicAgreement())
                .with(basicPayment())
                .with(basicDocument())
                .with(basicHealthQuestionnaire())
                .with(BeneficiaryBuilder
                        .with(basicBeneficiary())
                        .with(basicAddress1()).build())
                .with(CoverageBuilder
                        .with(basicCoverage())
                        .with(basicRisk()).build())
                .with(PartyBuilder
                        .with(basicParty())
                        .with(basicAddress2())
                        .with(basicContact()).build())
                .build();

        agreementRepository.save(agreement);
    }

    @Test
    public void shouldUpdateAgreementStatus() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        HttpEntity<String> request = new HttpEntity<>("{ \"id\": 1, \"status\": \"steady\" }", headers);

        ResponseEntity<String> response = restTemplate.exchange("/agreements/1", HttpMethod.PATCH, request, String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        Agreement dbAgreement = agreementRepository.findById(1L).get();
        assertThat(dbAgreement.getStatus()).isEqualTo("steady");
    }


}
