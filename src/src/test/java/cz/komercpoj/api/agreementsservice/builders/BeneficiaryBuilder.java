package cz.komercpoj.api.agreementsservice.builders;

import cz.komercpoj.api.agreementsservice.model.Address;
import cz.komercpoj.api.agreementsservice.model.Beneficiary;

import java.util.HashSet;

public class BeneficiaryBuilder {
    private Beneficiary beneficiary;

    private BeneficiaryBuilder() {
    }

    public static BeneficiaryBuilder with(Beneficiary beneficiary) {
        BeneficiaryBuilder beneficiaryBuilder = new BeneficiaryBuilder();
        beneficiaryBuilder.beneficiary = beneficiary;
        return beneficiaryBuilder;
    }

    public BeneficiaryBuilder with(Address address) {
        if (beneficiary.getAddresses() == null) {
            beneficiary.setAddresses(new HashSet<>());
        }

        beneficiary.getAddresses().add(address);
        return this;
    }

    public Beneficiary build() {
        return beneficiary;
    }
}
