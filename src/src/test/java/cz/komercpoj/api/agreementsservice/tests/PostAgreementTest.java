package cz.komercpoj.api.agreementsservice.tests;

import cz.komercpoj.api.agreementsservice.model.Address;
import cz.komercpoj.api.agreementsservice.model.Agreement;
import cz.komercpoj.api.agreementsservice.model.Beneficiary;
import cz.komercpoj.api.agreementsservice.model.Contact;
import cz.komercpoj.api.agreementsservice.model.Coverage;
import cz.komercpoj.api.agreementsservice.model.Document;
import cz.komercpoj.api.agreementsservice.model.HealthQuestionnaire;
import cz.komercpoj.api.agreementsservice.model.Party;
import cz.komercpoj.api.agreementsservice.model.Payment;
import cz.komercpoj.api.agreementsservice.repositories.AgreementRepository;
import io.restassured.path.json.JsonPath;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = {"spring.datasource.url=jdbc:h2:mem:PostAgreementDB"})
public class PostAgreementTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private AgreementRepository agreementRepository;

    @Test
    public void shouldReturnCreatedAgreementWithSubEntities() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        HttpEntity<String> request = new HttpEntity<>(creationJson, headers);

        ResponseEntity<String> response = restTemplate.postForEntity("/agreements", request, String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        JsonPath jsonPath = JsonPath.with(response.getBody());
        // checking only couple of attributes, serialization is checked by GET test and created content in DB content test
        assertThat(jsonPath.getLong("agreementNumber")).isEqualTo(5678L);
        assertThat(jsonPath.getString("agreementBeginDate")).isEqualTo("2017-06-11");
        assertThat(jsonPath.getString("agreementEndDate")).isNull();
        assertThat(jsonPath.getString("bruttoPremium")).isEqualTo("no idea");
        assertThat(jsonPath.getString("payment.bankAccountNumber")).isEqualTo("baccnxx");
        assertThat(jsonPath.getString("payment.frequency")).isEqualTo("daily");
        assertThat(jsonPath.getString("document.type")).isEqualTo("passport");
        assertThat(jsonPath.getString("document.issueDate")).isEqualTo("2010-11-08");
        assertThat(jsonPath.getString("coverages[0].sumInsured")).isEqualTo("1000000");
        assertThat(jsonPath.getString("coverages[0].risk.code")).isEqualTo("qq01");
        assertThat(jsonPath.getString("healthQuestionnaire.weight")).isEqualTo("127kg");
        assertThat(jsonPath.getString("beneficiaries[0].firstName")).isEqualTo("Jane");
        assertThat(jsonPath.getString("beneficiaries[0].ratio")).isEqualTo("0.8");
        assertThat(jsonPath.getString("beneficiaries[0].addresses[0].street")).isEqualTo("Štěpánská");
        assertThat(jsonPath.getString("beneficiaries[0].addresses[0].provisionalNumber")).isNull();
        assertThat(jsonPath.getString("parties[0].surname")).isEqualTo("Orwell");
        assertThat(jsonPath.getString("parties[0].birthDate")).isEqualTo("1964-08-22");
        assertThat(jsonPath.getString("parties[0].addresses[0].addressType")).isEqualTo("red district");
        assertThat(jsonPath.getString("parties[0].addresses[0].country")).isEqualTo("Liberia");
        assertThat(jsonPath.getString("parties[0].contacts[0].contactType")).isEqualTo("snail mail");
        assertThat(jsonPath.getString("parties[0].incomeAmount")).isEqualTo("100000€");
    }

    @Test
    @Transactional // without this, lazy getting of one-to-many relations of Agreement fails
    public void shouldCreatedAgreementWithSubEntitiesInDatabase() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        HttpEntity<String> request = new HttpEntity<>(creationJson, headers);

        ResponseEntity<String> response = restTemplate.postForEntity("/agreements", request, String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        Long createdAgreementId = JsonPath.with(response.getBody()).getLong("id");

        Agreement dbAgreement = agreementRepository.findById(createdAgreementId).get();
        assertThat(dbAgreement.getAgreementNumber()).isEqualTo(5678L);
        assertThat(dbAgreement.getAgreementBeginDate()).isEqualTo(LocalDate.of(2017,6,11));
        assertThat(dbAgreement.getInsurancePeriod()).isEqualTo("century");
        assertThat(dbAgreement.getInsuranceTermsVersion()).isEqualTo("11");
        assertThat(dbAgreement.getSignDate()).isEqualTo(LocalDate.of(2017,6,2));
        assertThat(dbAgreement.getBruttoPremium()).isEqualTo("no idea");
        assertThat(dbAgreement.getPremium()).isEqualTo("idk");

        HealthQuestionnaire healthQuestionnaire = dbAgreement.getHealthQuestionnaire();
        assertThat(healthQuestionnaire.getWeight()).isEqualTo("127kg");
        assertThat(healthQuestionnaire.getHeight()).isEqualTo("155cm");

        Payment payment = dbAgreement.getPayment();
        assertThat(payment.getBankCode()).isEqualTo("bcodexx");
        assertThat(payment.getMethod()).isEqualTo("cardxx");
        assertThat(payment.getBankAccountNumber()).isEqualTo("baccnxx");
        assertThat(payment.getFrequency()).isEqualTo("daily");

        Document document = dbAgreement.getDocument();
        assertThat(document.getType()).isEqualTo("passport");
        assertThat(document.getNumber()).isEqualTo("8877");
        assertThat(document.getIssueDate()).isEqualTo(LocalDate.of(2010,11,8));
        assertThat(document.getExpirationDate()).isEqualTo(LocalDate.of(2025,3,17));
        assertThat(document.getIssueState()).isEqualTo("issuedxx");
        assertThat(document.getIssuedBy()).isEqualTo("KGB");

        Coverage coverage = dbAgreement.getCoverages().iterator().next();
        assertThat(coverage.getSumInsured()).isEqualTo("1000000");
        assertThat(coverage.getPremium()).isEqualTo("always");
        assertThat(coverage.getRisk().getCode()).isEqualTo("qq01");

        Beneficiary beneficiary = dbAgreement.getBeneficiaries().iterator().next();
        assertThat(beneficiary.getFirstName()).isEqualTo("Jane");
        assertThat(beneficiary.getSurname()).isEqualTo("Dee");
        assertThat(beneficiary.getBirthDate()).isEqualTo(LocalDate.of(1971,7,12));
        assertThat(beneficiary.getRelationship()).isEqualTo("bro");
        assertThat(beneficiary.getRatio()).isEqualTo("0.8");

        Address beneficiaryAddress = beneficiary.getAddresses().iterator().next();
        assertThat(beneficiaryAddress.getAddressType()).isEqualTo("orbit");
        assertThat(beneficiaryAddress.getStreet()).isEqualTo("Štěpánská");
        assertThat(beneficiaryAddress.getConscriptionNumber()).isEqualTo("99");
        assertThat(beneficiaryAddress.getStreetNumber()).isEqualTo("19");
        assertThat(beneficiaryAddress.getProvisionalNumber()).isNull();
        assertThat(beneficiaryAddress.getCity()).isEqualTo("Praxa");
        assertThat(beneficiaryAddress.getPostCode()).isEqualTo("111 00");
        assertThat(beneficiaryAddress.getCountry()).isEqualTo("Bangladesh");

        Party party = dbAgreement.getParties().iterator().next();
        assertThat(party.getTitle()).isEqualTo("ing.");
        assertThat(party.getName()).isEqualTo("George");
        assertThat(party.getSurname()).isEqualTo("Orwell");
        assertThat(party.getSex()).isEqualTo("yes please");
        assertThat(party.getBirthName()).isNull();
        assertThat(party.getTitleAfter()).isEqualTo("phd.");
        assertThat(party.getBirthDate()).isEqualTo(LocalDate.of(1964,8,22));
        assertThat(party.getBirthNumber()).isEqualTo("124589");
        assertThat(party.getBirthPlace()).isEqualTo("Venus");
        assertThat(party.getBirthState()).isEqualTo("Venusia");
        assertThat(party.getWorkloadType()).isEqualTo("light");
        assertThat(party.getIncomeAmount()).isEqualTo("100000€");
        assertThat(party.getJob()).isEqualTo("rentier");

        Address partyAddress = party.getAddresses().iterator().next();
        assertThat(partyAddress.getAddressType()).isEqualTo("red district");
        assertThat(partyAddress.getStreet()).isEqualTo("Ve Smečkách");
        assertThat(partyAddress.getConscriptionNumber()).isEqualTo("115");
        assertThat(partyAddress.getStreetNumber()).isEqualTo("13");
        assertThat(partyAddress.getProvisionalNumber()).isNull();
        assertThat(partyAddress.getCity()).isEqualTo("Prahahaha");
        assertThat(partyAddress.getPostCode()).isEqualTo("101 00");
        assertThat(partyAddress.getCountry()).isEqualTo("Liberia");

        Contact contact = party.getContacts().iterator().next();
        assertThat(contact.getContactType()).isEqualTo("snail mail");
    }

    private static final String creationJson = "{ " +
            "  \"agreementNumber\": 5678, " +
            "  \"agreementBeginDate\": \"2017-06-11\", " +
            "  \"insurancePeriod\": \"century\", " +
            "  \"insuranceTermsVersion\": \"11\", " +
            "  \"signDate\": \"2017-06-02\", " +
            "  \"payment\": { " +
            "    \"bankCode\": \"bcodexx\", " +
            "    \"method\": \"cardxx\", " +
            "    \"bankAccountNumber\": \"baccnxx\", " +
            "    \"frequency\": \"daily\" " +
            "  }, " +
            "  \"document\": { " +
            "    \"type\": \"passport\", " +
            "    \"number\": \"8877\", " +
            "    \"issueDate\": \"2010-11-08\", " +
            "    \"expirationDate\": \"2025-03-17\", " +
            "    \"issueState\": \"issuedxx\", " +
            "    \"issuedBy\": \"KGB\" " +
            "  }, " +
            "  \"parties\": [{ " +
            "    \"title\": \"ing.\", " +
            "    \"name\": \"George\", " +
            "    \"surname\": \"Orwell\", " +
            "    \"sex\": \"yes please\", " +
            "    \"birthName\": null, " +
            "    \"titleAfter\": \"phd.\", " +
            "    \"birthDate\": \"1964-08-22\", " +
            "    \"birthNumber\": \"124589\", " +
            "    \"birthPlace\": \"Venus\", " +
            "    \"birthState\": \"Venusia\", " +
            "    \"addresses\": [{ " +
            "      \"addressType\": \"red district\", " +
            "      \"street\": \"Ve Smečkách\", " +
            "      \"conscriptionNumber\": \"115\", " +
            "      \"streetNumber\": \"13\", " +
            "      \"city\": \"Prahahaha\", " +
            "      \"postCode\": \"101 00\", " +
            "      \"country\": \"Liberia\" " +
            "    }], " +
            "    \"contacts\": [{ " +
            "      \"contactType\": \"snail mail\" " +
            "    }], " +
            "    \"workloadType\": \"light\", " +
            "    \"incomeAmount\": \"100000€\", " +
            "    \"job\": \"rentier\" " +
            "  }], " +
            "  \"coverages\": [{ " +
            "    \"sumInsured\": \"1000000\", " +
            "    \"premium\": \"always\", " +
            "    \"risk\": { " +
            "      \"code\": \"qq01\" " +
            "    } " +
            "  }], " +
            "  \"beneficiaries\": [{ " +
            "    \"firstName\": \"Jane\", " +
            "    \"surname\": \"Dee\", " +
            "    \"birthDate\": \"1971-07-12\", " +
            "    \"relationship\": \"bro\", " +
            "    \"ratio\": \"0.8\", " +
            "    \"addresses\": [{ " +
            "      \"addressType\": \"orbit\", " +
            "      \"street\": \"Štěpánská\", " +
            "      \"conscriptionNumber\": \"99\", " +
            "      \"streetNumber\": \"19\", " +
            "      \"city\": \"Praxa\", " +
            "      \"postCode\": \"111 00\", " +
            "      \"country\": \"Bangladesh\" " +
            "    }] " +
            "  }], " +
            "  \"healthQuestionnaire\": { " +
            "    \"weight\": \"127kg\", " +
            "    \"height\": \"155cm\" " +
            "  }, " +
            "  \"bruttoPremium\": \"no idea\", " +
            "  \"premium\": \"idk\" " +
            "}";
}
