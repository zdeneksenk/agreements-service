package cz.komercpoj.api.agreementsservice.tests;

import cz.komercpoj.api.agreementsservice.builders.AgreementBuilder;
import cz.komercpoj.api.agreementsservice.builders.BeneficiaryBuilder;
import cz.komercpoj.api.agreementsservice.builders.CoverageBuilder;
import cz.komercpoj.api.agreementsservice.builders.PartyBuilder;
import cz.komercpoj.api.agreementsservice.exceptions.EntityIdConflictException;
import cz.komercpoj.api.agreementsservice.model.Agreement;
import cz.komercpoj.api.agreementsservice.model.Payment;
import cz.komercpoj.api.agreementsservice.repositories.AgreementRepository;
import ma.glasnost.orika.MapperFacade;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicAddress1;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicAddress2;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicAgreement;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicBeneficiary;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicContact;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicCoverage;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicDocument;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicHealthQuestionnaire;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicParty;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicPayment;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicRisk;
import static cz.komercpoj.api.agreementsservice.data.UpdateTestData.updatedAgreement;
import static cz.komercpoj.api.agreementsservice.data.UpdateTestData.updatedPayment;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.datasource.url=jdbc:h2:mem:MappingDB"})
public class MappingTest {

    @Autowired
    private MapperFacade mapperFacade;

    @Autowired
    private AgreementRepository agreementRepository;

    private static boolean isInitialized;

    @Before
    public void setUp() {
        if (isInitialized) {
            return;
        }

        isInitialized = true;
        loadData();
    }

    private void loadData() {
        Agreement agreement = AgreementBuilder.with(basicAgreement())
                .with(basicPayment())
                .with(basicDocument())
                .with(basicHealthQuestionnaire())
                .with(BeneficiaryBuilder
                        .with(basicBeneficiary())
                        .with(basicAddress1()).build())
                .with(CoverageBuilder
                        .with(basicCoverage())
                        .with(basicRisk()).build())
                .with(PartyBuilder
                        .with(basicParty())
                        .with(basicAddress2())
                        .with(basicContact()).build())
                .build();

        agreementRepository.save(agreement);
    }

    @Test
    public void shouldExcludeAgreementId() {
        Agreement agreementIn = new Agreement();
        agreementIn.setId(55L);

        Agreement agreementFromDB = agreementRepository.findById(1L).get();

        mapperFacade.map(agreementIn, agreementFromDB);

        assertThat(agreementFromDB.getId()).isEqualTo(1L);
    }

    @Test
    public void shouldMapBasicAgreementFields() {
        Agreement agreementIn = updatedAgreement();
        Agreement agreementFromDB = agreementRepository.findById(1L).get();

        mapperFacade.map(agreementIn, agreementFromDB);
        agreementRepository.save(agreementFromDB);

        Agreement updatedAgreementFromDB = agreementRepository.findById(1L).get();
        assertThat(updatedAgreementFromDB.getAgreementNumber()).isEqualTo(123888L);
        assertThat(updatedAgreementFromDB.getAgreementBeginDate()).isEqualTo(LocalDate.of(2017,5,12));
        assertThat(updatedAgreementFromDB.getBruttoPremium()).isEqualTo("bruPreUpd");
        assertThat(updatedAgreementFromDB.getInsurancePeriod()).isEqualTo("month");
        assertThat(updatedAgreementFromDB.getInsuranceTermsVersion()).isEqualTo("4");
        assertThat(updatedAgreementFromDB.getPremium()).isNull();
        assertThat(updatedAgreementFromDB.getSignDate()).isEqualTo(LocalDate.of(2017,5,9));
        assertThat(updatedAgreementFromDB.getStatus()).isEqualTo("nice");
    }

    @Test
    public void shouldCreateNewPayment_whenIncomingHasEmptyId() {
        Agreement agreementIn =  AgreementBuilder.with(updatedAgreement())
                .with(updatedPayment())
                .build();
        Agreement agreementFromDB = agreementRepository.findById(1L).get();
        Long originalPaymentId = agreementFromDB.getPayment().getId();

        mapperFacade.map(agreementIn, agreementFromDB);
        assertThat(agreementFromDB.getPayment().getId()).isNull();

        agreementRepository.save(agreementFromDB);

        Agreement updatedAgreementFromDB = agreementRepository.findById(1L).get();
        Payment newPayment = updatedAgreementFromDB.getPayment();
        assertThat(newPayment.getId()).isNotNull();
        assertThat(newPayment.getId()).isNotEqualTo(originalPaymentId);
        assertThat(newPayment.getBankAccountNumber()).isEqualTo("baccnUpd");
        assertThat(newPayment.getBankCode()).isEqualTo("bcodeUpd");
        assertThat(newPayment.getFrequency()).isEqualTo("year");
        assertThat(newPayment.getMethod()).isEqualTo("transfer");
    }

    @Test
    public void shouldUpdatePayment_whenIncomingHasSameId() {
        Agreement agreementIn =  AgreementBuilder.with(updatedAgreement())
                .with(updatedPayment())
                .build();
        Agreement agreementFromDB = agreementRepository.findById(1L).get();
        agreementIn.getPayment().setId(agreementFromDB.getPayment().getId());
        Long originalPaymentId = agreementFromDB.getPayment().getId();

        mapperFacade.map(agreementIn, agreementFromDB);
        assertThat(agreementFromDB.getPayment().getId()).isNotNull();

        agreementRepository.save(agreementFromDB);
        Agreement updatedAgreementFromDB = agreementRepository.findById(1L).get();

        Payment updatedPayment = updatedAgreementFromDB.getPayment();
        assertThat(updatedPayment.getId()).isNotNull();
        assertThat(updatedPayment.getId()).isEqualTo(originalPaymentId);
        assertThat(updatedPayment.getBankAccountNumber()).isEqualTo("baccnUpd");
        assertThat(updatedPayment.getBankCode()).isEqualTo("bcodeUpd");
        assertThat(updatedPayment.getFrequency()).isEqualTo("year");
        assertThat(updatedPayment.getMethod()).isEqualTo("transfer");
    }

    @Test
    public void shouldRefuseMappingOfOneToOne_WithDifferentId() {
        Agreement agreementIn =  AgreementBuilder.with(updatedAgreement())
                .with(updatedPayment())
                .build();
        agreementIn.getPayment().setId(20L);

        Agreement agreementFromDB = agreementRepository.findById(1L).get();

        try {
            mapperFacade.map(agreementIn, agreementFromDB);
            failBecauseExceptionWasNotThrown(EntityIdConflictException.class);
        } catch (EntityIdConflictException e) {
            assertThat(e.getMessage()).isEqualTo("Payment - payload id 20 does not match entity id 1");
        }
    }


}
