package cz.komercpoj.api.agreementsservice.tests;

import cz.komercpoj.api.agreementsservice.builders.AgreementBuilder;
import cz.komercpoj.api.agreementsservice.builders.BeneficiaryBuilder;
import cz.komercpoj.api.agreementsservice.builders.CoverageBuilder;
import cz.komercpoj.api.agreementsservice.builders.PartyBuilder;
import cz.komercpoj.api.agreementsservice.model.Agreement;
import cz.komercpoj.api.agreementsservice.repositories.AgreementRepository;
import io.restassured.path.json.JsonPath;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicAddress1;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicAddress2;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicAgreement;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicBeneficiary;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicContact;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicCoverage;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicDocument;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicHealthQuestionnaire;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicParty;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicPayment;
import static cz.komercpoj.api.agreementsservice.data.BasicTestData.basicRisk;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, properties = {"spring.datasource.url=jdbc:h2:mem:GetAgreementDB"})
public class GetAgreementTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private AgreementRepository agreementRepository;

	private static boolean isInitialized;

	@Before
	public void setUp() {
		if (isInitialized) {
			return;
		}

		isInitialized = true;
		loadData();
	}

	private void loadData() {
		Agreement agreement = AgreementBuilder.with(basicAgreement())
				.with(basicPayment())
				.with(basicDocument())
				.with(basicHealthQuestionnaire())
				.with(BeneficiaryBuilder
						.with(basicBeneficiary())
						.with(basicAddress1()).build())
				.with(CoverageBuilder
						.with(basicCoverage())
						.with(basicRisk()).build())
				.with(PartyBuilder
						.with(basicParty())
						.with(basicAddress2())
						.with(basicContact()).build())
				.build();

		agreementRepository.save(agreement);
	}

	@Test
	public void shouldReturnNotFound_forNonExistingAgreementId() {
		ResponseEntity<String> response = restTemplate.getForEntity("/agreements/11223399", String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

		JsonPath jsonPath = JsonPath.with(response.getBody());
		assertThat(jsonPath.getString("message")).isEqualTo("Could not find Agreement with id 11223399");
	}

	@Test
	public void shouldReturnAgreementBasicValues() {
		ResponseEntity<String> response = restTemplate.getForEntity("/agreements/1", String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

		JsonPath jsonPath = JsonPath.with(response.getBody());
		assertThat(jsonPath.getLong("id")).isEqualTo(1L);
		assertThat(jsonPath.getLong("agreementNumber")).isEqualTo(123456L);
		assertThat(jsonPath.getString("agreementBeginDate")).isEqualTo("2017-05-10");
		assertThat(jsonPath.getString("agreementEndDate")).isNull();
		assertThat(jsonPath.getString("bruttoPremium")).isEqualTo("bruPre");
		assertThat(jsonPath.getString("insurancePeriod")).isEqualTo("year");
		assertThat(jsonPath.getString("insuranceTermsVersion")).isEqualTo("3.5");
		assertThat(jsonPath.getString("premium")).isEqualTo("yess!");
		assertThat(jsonPath.getString("signDate")).isEqualTo("2017-05-07");
	}

	@Test
	public void shouldReturnAgreementPayment() {
		ResponseEntity<String> response = restTemplate.getForEntity("/agreements/1", String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

		JsonPath jsonPath = JsonPath.with(response.getBody()).setRoot("payment");
		assertThat(jsonPath.getLong("id")).isEqualTo(1L);
		assertThat(jsonPath.getString("bankAccountNumber")).isEqualTo("baccn");
		assertThat(jsonPath.getString("bankCode")).isEqualTo("bcode");
		assertThat(jsonPath.getString("frequency")).isEqualTo("month");
		assertThat(jsonPath.getString("method")).isEqualTo("card");
	}

	@Test
	public void shouldReturnAgreementDocument() {
		ResponseEntity<String> response = restTemplate.getForEntity("/agreements/1", String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

		JsonPath jsonPath = JsonPath.with(response.getBody()).setRoot("document");
		assertThat(jsonPath.getLong("id")).isEqualTo(1L);
		assertThat(jsonPath.getString("expirationDate")).isEqualTo("2019-05-07");
		assertThat(jsonPath.getString("issueDate")).isEqualTo("2017-05-06");
		assertThat(jsonPath.getString("issuedBy")).isEqualTo("Fantomas");
		assertThat(jsonPath.getString("issueState")).isEqualTo("issued");
		assertThat(jsonPath.getString("number")).isEqualTo("1234");
		assertThat(jsonPath.getString("type")).isEqualTo("contract");
	}

	@Test
	public void shouldReturnAgreementHealthQuestionnaire() {
		ResponseEntity<String> response = restTemplate.getForEntity("/agreements/1", String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

		JsonPath jsonPath = JsonPath.with(response.getBody()).setRoot("healthQuestionnaire");
		assertThat(jsonPath.getLong("id")).isEqualTo(1L);
		assertThat(jsonPath.getString("height")).isEqualTo("182cm");
		assertThat(jsonPath.getString("weight")).isEqualTo("85kg");
	}

	@Test
	public void shouldReturnAgreementBeneficiaryWithAddress() {
		ResponseEntity<String> response = restTemplate.getForEntity("/agreements/1", String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

		JsonPath jsonPath = JsonPath.with(response.getBody()).setRoot("beneficiaries[0]");
		assertThat(jsonPath.getLong("id")).isEqualTo(1L);
		assertThat(jsonPath.getString("birthDate")).isEqualTo("1972-12-15");
		assertThat(jsonPath.getString("firstName")).isEqualTo("John");
		assertThat(jsonPath.getString("surname")).isEqualTo("Doe");
		assertThat(jsonPath.getString("relationship")).isEqualTo("grand cousin");
		assertThat(jsonPath.getString("ratio")).isEqualTo("0.7");

		jsonPath.setRoot("beneficiaries[0].addresses[0]");
		// id not checked as order of creation is not guaranteed and there is also party address
		assertThat(jsonPath.getString("addressType")).isEqualTo("perm");
		assertThat(jsonPath.getString("city")).isEqualTo("Praha");
		assertThat(jsonPath.getString("conscriptionNumber")).isEqualTo("95");
		assertThat(jsonPath.getString("country")).isEqualTo("Burma");
		assertThat(jsonPath.getString("postCode")).isEqualTo("112 00");
		assertThat(jsonPath.getString("street")).isEqualTo("Matiční");
		assertThat(jsonPath.getString("streetNumber")).isEqualTo("18");
	}

	@Test
	public void shouldReturnAgreementCoverageWithRisk() {
		ResponseEntity<String> response = restTemplate.getForEntity("/agreements/1", String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

		JsonPath jsonPath = JsonPath.with(response.getBody()).setRoot("coverages[0]");
		assertThat(jsonPath.getLong("id")).isEqualTo(1L);
		assertThat(jsonPath.getString("premium")).isEqualTo("maybe");
		assertThat(jsonPath.getString("sumInsured")).isEqualTo("sins");

		jsonPath.setRoot("coverages[0].risk");
		assertThat(jsonPath.getLong("id")).isEqualTo(1L);
		assertThat(jsonPath.getString("code")).isEqualTo("uff");
	}

	@Test
	public void shouldReturnAgreementPartyWithAddressAndContact() {
		ResponseEntity<String> response = restTemplate.getForEntity("/agreements/1", String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

		System.out.println(response.getBody());
		JsonPath jsonPath = JsonPath.with(response.getBody()).setRoot("parties[0]");
		assertThat(jsonPath.getLong("id")).isEqualTo(1L);
		assertThat(jsonPath.getString("title")).isEqualTo("pxdr");
		assertThat(jsonPath.getString("name")).isEqualTo("Jim");
		assertThat(jsonPath.getString("surname")).isEqualTo("Doesnt");
		assertThat(jsonPath.getString("sex")).isEqualTo("et");
		assertThat(jsonPath.getString("birthName")).isEqualTo("Matter");
		assertThat(jsonPath.getString("titleAfter")).isEqualTo("mhm");
		assertThat(jsonPath.getString("birthDate")).isEqualTo("1969-02-08");
		assertThat(jsonPath.getString("birthNumber")).isEqualTo("12345");
		assertThat(jsonPath.getString("birthPlace")).isEqualTo("Mars");
		assertThat(jsonPath.getString("birthState")).isEqualTo("Martianistan");
		assertThat(jsonPath.getString("workloadType")).isEqualTo("heavy");
		assertThat(jsonPath.getString("incomeAmount")).isEqualTo("10000€");
		assertThat(jsonPath.getString("job")).isEqualTo("Earth Director");

		jsonPath.setRoot("parties[0].addresses[0]");
		// id not checked as order of creation is not guaranteed and there is also beneficiary address
		assertThat(jsonPath.getString("addressType")).isEqualTo("tmp");
		assertThat(jsonPath.getString("city")).isEqualTo("Prahaha");
		assertThat(jsonPath.getString("conscriptionNumber")).isEqualTo("950");
		assertThat(jsonPath.getString("country")).isEqualTo("Sierra Leone");
		assertThat(jsonPath.getString("postCode")).isEqualTo("113 00");
		assertThat(jsonPath.getString("street")).isEqualTo("Pařížská");
		assertThat(jsonPath.getString("streetNumber")).isEqualTo("55");

		jsonPath.setRoot("parties[0].contacts[0]");
		assertThat(jsonPath.getLong("id")).isEqualTo(1L);
		assertThat(jsonPath.getString("contactType")).isEqualTo("email");
	}

}
