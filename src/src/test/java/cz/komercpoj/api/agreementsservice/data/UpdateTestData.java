package cz.komercpoj.api.agreementsservice.data;

import cz.komercpoj.api.agreementsservice.model.Agreement;
import cz.komercpoj.api.agreementsservice.model.Payment;

import java.time.LocalDate;

public class UpdateTestData {
    public static Agreement updatedAgreement() {
        Agreement agreement = new Agreement();
        agreement.setId(1L);
        agreement.setAgreementNumber(123888L);
        agreement.setAgreementBeginDate(LocalDate.of(2017, 5, 12));
        agreement.setBruttoPremium("bruPreUpd");
        agreement.setInsurancePeriod("month");
        agreement.setInsuranceTermsVersion("4");
        //premium intentionally null
        agreement.setSignDate(LocalDate.of(2017, 5, 9));
        agreement.setStatus("nice");
        return agreement;
    }

    public static Payment updatedPayment() {
        Payment payment = new Payment();
        payment.setBankAccountNumber("baccnUpd");
        payment.setBankCode("bcodeUpd");
        payment.setFrequency("year");
        payment.setMethod("transfer");
        return payment;
    }
}
