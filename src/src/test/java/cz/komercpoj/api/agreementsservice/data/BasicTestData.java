package cz.komercpoj.api.agreementsservice.data;

import cz.komercpoj.api.agreementsservice.model.Address;
import cz.komercpoj.api.agreementsservice.model.Agreement;
import cz.komercpoj.api.agreementsservice.model.Beneficiary;
import cz.komercpoj.api.agreementsservice.model.Contact;
import cz.komercpoj.api.agreementsservice.model.Coverage;
import cz.komercpoj.api.agreementsservice.model.Document;
import cz.komercpoj.api.agreementsservice.model.HealthQuestionnaire;
import cz.komercpoj.api.agreementsservice.model.Party;
import cz.komercpoj.api.agreementsservice.model.Payment;
import cz.komercpoj.api.agreementsservice.model.Risk;

import java.time.LocalDate;

public class BasicTestData {

    public static Agreement basicAgreement() {
        Agreement agreement = new Agreement();
        agreement.setAgreementNumber(123456L);
        agreement.setAgreementBeginDate(LocalDate.of(2017, 5, 10));
        agreement.setBruttoPremium("bruPre");
        agreement.setInsurancePeriod("year");
        agreement.setInsuranceTermsVersion("3.5");
        agreement.setPremium("yess!");
        agreement.setSignDate(LocalDate.of(2017, 5, 7));
        agreement.setStatus("ready");
        return agreement;
    }

    public static Payment basicPayment() {
        Payment payment = new Payment();
        payment.setBankAccountNumber("baccn");
        payment.setBankCode("bcode");
        payment.setFrequency("month");
        payment.setMethod("card");
        return payment;
    }

    public static Document basicDocument() {
        Document document = new Document();
        document.setExpirationDate(LocalDate.of(2019, 5, 7));
        document.setIssueDate(LocalDate.of(2017, 5, 6));
        document.setIssuedBy("Fantomas");
        document.setIssueState("issued");
        document.setNumber("1234");
        document.setType("contract");
        return document;
    }

    public static HealthQuestionnaire basicHealthQuestionnaire() {
        HealthQuestionnaire healthQuestionnaire = new HealthQuestionnaire();
        healthQuestionnaire.setHeight("182cm");
        healthQuestionnaire.setWeight("85kg");
        return healthQuestionnaire;
    }

    public static Beneficiary basicBeneficiary() {
        Beneficiary beneficiary = new Beneficiary();
        beneficiary.setBirthDate(LocalDate.of(1972, 12, 15));
        beneficiary.setFirstName("John");
        beneficiary.setSurname("Doe");
        beneficiary.setRelationship("grand cousin");
        beneficiary.setRatio("0.7");
        return beneficiary;
    }

    public static Address basicAddress1() {
        Address address = new Address();
        address.setAddressType("perm");
        address.setCity("Praha");
        address.setConscriptionNumber("95");
        address.setCountry("Burma");
        address.setPostCode("112 00");
        address.setStreet("Matiční");
        address.setStreetNumber("18");
        return address;
    }

    public static Address basicAddress2() {
        Address address = new Address();
        address.setAddressType("tmp");
        address.setCity("Prahaha");
        address.setConscriptionNumber("950");
        address.setCountry("Sierra Leone");
        address.setPostCode("113 00");
        address.setStreet("Pařížská");
        address.setStreetNumber("55");
        return address;
    }

    public static Risk basicRisk() {
        Risk risk = new Risk();
        risk.setCode("uff");
        return risk;
    }

    public static Coverage basicCoverage() {
        Coverage coverage = new Coverage();
        coverage.setPremium("maybe");
        coverage.setSumInsured("sins");
        return coverage;
    }

    public static Party basicParty() {
        Party party = new Party();
        party.setTitle("pxdr");
        party.setName("Jim");
        party.setSurname("Doesnt");
        party.setSex("et");
        party.setBirthName("Matter");
        party.setTitleAfter("mhm");
        party.setBirthDate(LocalDate.of(1969, 2, 8));
        party.setBirthNumber("12345");
        party.setBirthPlace("Mars");
        party.setBirthState("Martianistan");
        party.setWorkloadType("heavy");
        party.setIncomeAmount("10000€");
        party.setJob("Earth Director");
        return party;
    }

    public static Contact basicContact() {
        Contact contact = new Contact();
        contact.setContactType("email");
        return contact;
    }
}
