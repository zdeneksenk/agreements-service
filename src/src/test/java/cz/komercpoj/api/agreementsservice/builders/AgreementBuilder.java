package cz.komercpoj.api.agreementsservice.builders;

import cz.komercpoj.api.agreementsservice.model.Agreement;
import cz.komercpoj.api.agreementsservice.model.Beneficiary;
import cz.komercpoj.api.agreementsservice.model.Coverage;
import cz.komercpoj.api.agreementsservice.model.Document;
import cz.komercpoj.api.agreementsservice.model.HealthQuestionnaire;
import cz.komercpoj.api.agreementsservice.model.Party;
import cz.komercpoj.api.agreementsservice.model.Payment;

import java.util.HashSet;

public class AgreementBuilder {
    private Agreement agreement;

    private AgreementBuilder() {
    }

    public static AgreementBuilder with(Agreement agreement) {
        AgreementBuilder agreementBuilder = new AgreementBuilder();
        agreementBuilder.agreement = agreement;
        return agreementBuilder;
    }

    public AgreementBuilder with(Payment payment) {
        agreement.setPayment(payment);
        return this;
    }

    public AgreementBuilder with(Document document) {
        agreement.setDocument(document);
        return this;
    }

    public AgreementBuilder with(HealthQuestionnaire healthQuestionnaire) {
        agreement.setHealthQuestionnaire(healthQuestionnaire);
        return this;
    }

    public AgreementBuilder with(Beneficiary beneficiary) {
        if (agreement.getBeneficiaries() == null) {
            agreement.setBeneficiaries(new HashSet<>());
        }

        agreement.getBeneficiaries().add(beneficiary);
        return this;
    }

    public AgreementBuilder with(Coverage coverage) {
        if (agreement.getCoverages() == null) {
            agreement.setCoverages(new HashSet<>());
        }

        agreement.getCoverages().add(coverage);
        return this;
    }

    public AgreementBuilder with(Party party) {
        if (agreement.getParties() == null) {
            agreement.setParties(new HashSet<>());
        }

        agreement.getParties().add(party);
        return this;
    }

    public Agreement build() {
        return agreement;
    }
}
