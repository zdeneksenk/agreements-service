package cz.komercpoj.api.agreementsservice.builders;

import cz.komercpoj.api.agreementsservice.model.Address;
import cz.komercpoj.api.agreementsservice.model.Contact;
import cz.komercpoj.api.agreementsservice.model.Party;

import java.util.HashSet;

public class PartyBuilder {
    private Party party;

    private PartyBuilder() {
    }

    public static PartyBuilder with(Party party) {
        PartyBuilder partyBuilder = new PartyBuilder();
        partyBuilder.party = party;
        return partyBuilder;
    }

    public PartyBuilder with(Address address) {
        if (party.getAddresses() == null) {
            party.setAddresses(new HashSet<>());
        }

        party.getAddresses().add(address);
        return this;
    }

    public PartyBuilder with(Contact contact) {
        if (party.getContacts() == null) {
            party.setContacts(new HashSet<>());
        }

        party.getContacts().add(contact);
        return this;
    }

    public Party build() {
        return party;
    }
}
