package cz.komercpoj.api.agreementsservice.builders;

import cz.komercpoj.api.agreementsservice.model.Coverage;
import cz.komercpoj.api.agreementsservice.model.Risk;

public class CoverageBuilder {
    private Coverage coverage;

    private CoverageBuilder() {
    }

    public static CoverageBuilder with(Coverage coverage) {
        CoverageBuilder coverageBuilder = new CoverageBuilder();
        coverageBuilder.coverage = coverage;
        return coverageBuilder;
    }

    public CoverageBuilder with(Risk risk) {
        coverage.setRisk(risk);
        return this;
    }

    public Coverage build() {
        return coverage;
    }
}
